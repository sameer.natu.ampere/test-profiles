#!/bin/sh

echo "#!/bin/bash
for i in \$(seq 2 \${NUM_CPU_CORES}); do
	tar -zcf to-compress_\${i}.tar.gz to-compress &
done
tar -zcf to-compress.tar.gz to-compress
sleep 1

for i in \$(seq 1 \${NUM_CPU_CORES}); do
	rm -f to-compress_\${i}.tar.gz &
done
sleep 1" > compress-gzip-ampere
chmod +x compress-gzip-ampere


