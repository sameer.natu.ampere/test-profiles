#!/bin/sh

tar -xjf ffmpeg-4.4.tar.bz2
mkdir ffmpeg_/

cd ffmpeg-4.4/
./configure --disable-zlib --disable-doc --prefix=$HOME/ffmpeg_/
make -j $NUM_CPU_CORES
echo $? > ~/install-exit-status
make install
cd ~/
rm -rf ffmpeg-4.4/
rm -rf ffmpeg_/lib/

echo "#!/bin/bash

for i in \$(seq 1 \${NUM_CPU_CORES}); do
	./ffmpeg_/bin/ffmpeg -i HD2-h264.ts -f rawvideo -y -threads 1 -target ntsc-dv /dev/null > /tmp/ffmpeg_\$i.log 2>&1 &
done

while [ \`ps ax | grep \"ffmpeg_/bin/ffmpeg\" | wc -l\` -gt 1 ]; do
	sleep 1
done

TOTAL_FPS=0
for i in \$(seq 1 \${NUM_CPU_CORES}); do
	FPS=\`cat /tmp/ffmpeg_\${i}.log | grep \"fps=\" | tail -n1 | sed -n -e 's/^.*fps=//p' | sed \"s/^ //g\" | cut -d\" \" -f1\`
	TOTAL_FPS=\$((\${FPS}+\${TOTAL_FPS}))
done

echo \"Performance: \${TOTAL_FPS} fps\" > \$LOG_FILE

echo \$? > ~/test-exit-status" > ffmpeg-ampere
chmod +x ffmpeg-ampere
