#!/bin/sh

tar -xf x265_3.4.tar.gz
cd x265_3.4/build
cmake ../source
make -j $NUM_CPU_CORES
echo $? > ~/install-exit-status
cd ~

7z x Bosphorus_3840x2160_120fps_420_8bit_YUV_Y4M.7z -aoa
7z x Bosphorus_1920x1080_120fps_420_8bit_YUV_Y4M.7z -aoa

echo "#!/bin/bash
set -x

if [[ \"\$@\" == *\"1080\"* ]]; then
	NUM_INSTANCES=24
else
	NUM_INSTANCES=12
fi


for i in \$(seq 1 \${NUM_INSTANCES}); do
	./x265_3.4/build/x265 \$@ /dev/null > /tmp/\${i}.log 2>&1 &
done

while [ \`ps ax | grep \"x265_3.4/build/x265\" | wc -l\` -gt 1 ]; do
	sleep 1
done

fps_sum=0
for i in \$(seq 1 \${NUM_INSTANCES}); do
	fps=\`cat /tmp/\${i}.log | grep \"encoded\" | cut -d\"(\" -f2 | cut -d\")\" -f1 | cut -d\" \" -f1\`
	fps_sum=\`echo \"\${fps}+\${fps_sum}\" | bc -l\`
done


TIME_REQD=\`cat /tmp/1.log | grep \"^encoded\" | cut -d\" \" -f5 | tr -dc '[. [:digit:]]'\`
TIME_REQD_600F=\`echo \"600*\${NUM_INSTANCES}/\${TIME_REQD}\" | bc -l | xargs printf \"%.2f\"\`
RES_LINE_P2=\`cat /tmp/1.log | grep \"^encoded\" | cut -d\")\" -f2\`

cat /tmp/1.log | head -n -1 > \$LOG_FILE
echo \"encoded 600 frames in \${TIME_REQD_600F}s (\${fps_sum} fps)\${RES_LINE_P2}\" >> \$LOG_FILE

echo \$? > ~/test-exit-status" > x265-ampere
chmod +x x265-ampere
