#!/bin/sh

mkdir $HOME/nginx_

sudo apt install libpcre3-dev libexpat1-dev -y

tar -zxvf apache-ab-test-files-1.tar.gz
tar -zxvf nginx-1.18.0.tar.gz
tar -jxvf httpd-2.4.29.tar.bz2
tar -jxvf apr-util-1.6.1.tar.bz2
tar -jxvf apr-1.6.3.tar.bz2
mv apr-1.6.3 httpd-2.4.29/srclib/apr
mv apr-util-1.6.1 httpd-2.4.29/srclib/apr-util

# we need apache for ab, compile only apr,apt-utils,pcre and ab
cd httpd-2.4.29/
./configure --prefix=$HOME/httpd_ --enable-static-ab --without-http-cache
cd srclib/apr
make -j
cd ../apr-util
make -j
cd ../../support
make ab -j
cd ../..
cp -av httpd-2.4.29/support/ab nginx_/

cd nginx-1.18.0/
#echo "
#--- src/os/unix/ngx_user.c.orig
#+++ src/os/unix/ngx_user.c
#@@ -31,10 +31,6 @@ ngx_libc_crypt(ngx_pool_t *pool, u_char *key, u_char *salt, u_char **encrypted)
#     struct crypt_data   cd;
#
#     cd.initialized = 0;
#-#ifdef __GLIBC__
#-    /* work around the glibc bug */
#-    cd.current_salt[0] = ~salt[0];
#-#endif
#
#     value = crypt_r((char *) key, (char *) salt, &cd);
#" > REMOVE-WORKAROUND.patch

#patch -p0 < REMOVE-WORKAROUND.patch

#CFLAGS="-Wno-error -O3 -march=armv8.2-a -mtune=neoverse-n1 $CFLAGS" CXXFLAGS="-Wno-error -O3 -march=-march=armv8.2-a -mtune=neoverse-n1 $CFLAGS" ./configure --prefix=$HOME/nginx_ --without-http_rewrite_module --without-http-cache 

CFLAGS="-Wno-error -O3 -march=native $CFLAGS" CXXFLAGS="-Wno-error -O3 -march=native $CFLAGS" ./configure --prefix=$HOME/nginx_ --without-http_rewrite_module --without-http-cache

make -j $NUM_CPU_JOBS
echo $? > ~/install-exit-status
make install
cd ..
#rm -rf nginx-1.18.0/
#rm -rf httpd-2.4.29/

# patch listen port 80 -> 8088
echo "
--- nginx_/conf/nginx.conf.orig  2010-11-09 18:22:34.000000000 +0200
+++ nginx_/conf/nginx.conf       2010-11-09 18:17:14.000000000 +0200
@@ -33,7 +33,7 @@
     #gzip  on;

     server {
+        listen       8089;
-        listen       80;
         server_name  localhost;

         #charset koi8-r;
" > CHANGE-NGINX-PORT.patch

patch -p0 < CHANGE-NGINX-PORT.patch

#cp /opt/pts_essentials/nginx/nginx.conf /home/ubuntu/.phoronix-test-suite/installed-tests/pts/nginx-1.2.2/nginx_/conf/nginx.conf
mv -f test.html nginx_/html/
mv -f pts.png nginx_/html/

# ./nginx_/ab \$@ > \$LOG_FILE 2>&1
echo "#!/bin/bash
set -x
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx1.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx2.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx3.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx4.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx5.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx6.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx7.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx8.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx9.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx10.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx11.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx12.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx13.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx14.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx15.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx16.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx17.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx18.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx19.log 2>&1 &
./nginx_/ab -n 1000000 -c 18 http://127.0.0.1:8089/ > /tmp/nginx20.log 2>&1 &

ab_inst=\`pidof ab | wc -w\`
while [ \${ab_inst} -ne 0 ]; do
	sleep 5;
	ab_inst=\`pidof ab | wc -w\`;
done

sum=0;
for i in {1..20}; do
	curr_val=\`cat /tmp/nginx\${i}.log | grep \"Requests per second\" | cut -d\":\" -f2 | sed -e 's/ \+//g' | cut -d\"[\" -f1\`
	if [[ \"\${curr_val}\" != \"\" ]]; then
		sum=\`echo \"\${sum}+\${curr_val}\" | bc -l\`
	fi
done
cp /tmp/nginx1.log \${LOG_FILE}
sleep 1
sed -i \"/Requests per second:/c Requests per second:    \$sum [#/sec] (mean)\" \$LOG_FILE

#./nginx_/ab \$@ > \$LOG_FILE 2>&1
cp \$LOG_FILE /tmp/nginx.log
echo \$? > ~/test-exit-status" > nginx-ampere

chmod +x nginx-ampere
