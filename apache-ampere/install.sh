#!/bin/sh

mkdir $HOME/httpd_

tar -zxvf apache-ab-test-files-1.tar.gz
tar -jxvf httpd-2.4.29.tar.bz2
tar -jxvf apr-util-1.6.1.tar.bz2
tar -jxvf apr-1.6.3.tar.bz2
mv apr-1.6.3 httpd-2.4.29/srclib/apr
mv apr-util-1.6.1 httpd-2.4.29/srclib/apr-util

cd httpd-2.4.29/
./configure --prefix=$HOME/httpd_ --with-included-apr
make -j $NUM_CPU_JOBS
echo $? > ~/install-exit-status
make install
cd ~
rm -rf httpd-2.4.29/
rm -rf httpd_/manual/

patch -p0 < CHANGE-APACHE-PORT.patch
mv -f test.html httpd_/htdocs/
mv -f pts.png httpd_/htdocs/

#echo "#!/bin/sh
#./httpd_/bin/ab \$@ > \$LOG_FILE 2>&1
#echo \$? > ~/test-exit-status" > apache
echo "#!/bin/bash
set -x
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache1.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache2.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache3.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache4.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache5.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache6.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache7.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache8.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache9.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache10.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache11.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache12.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache13.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache14.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache15.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache16.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache17.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache18.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache19.log 2>&1 &
./httpd_/bin/ab -n 1000000 -c 36 http://127.0.0.1:8088/ > /tmp/apache20.log 2>&1 &

ab_inst=\`pidof ab | wc -w\`
while [ \${ab_inst} -ne 0 ]; do
        sleep 5;
        ab_inst=\`pidof ab | wc -w\`;
done

sum=0;
for i in {1..20}; do
        curr_val=\`cat /tmp/apache\${i}.log | grep \"Requests per second\" | cut -d\":\" -f2 | sed -e 's/ \+//g' | cut -d\"[\" -f1\`
        if [[ \"\${curr_val}\" != \"\" ]]; then
                sum=\`echo \"\${sum}+\${curr_val}\" | bc -l\`
        fi
done
cp /tmp/apache1.log \${LOG_FILE}
sleep 1
sed -i \"/Requests per second:/c Requests per second:    \$sum [#/sec] (mean)\" \$LOG_FILE

#./httpd_/bin/ab \$@ > \$LOG_FILE 2>&1
cp \$LOG_FILE /tmp/apache.log
echo \$? > ~/test-exit-status" > apache-ampere
chmod +x apache-ampere
