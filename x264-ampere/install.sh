#!/bin/sh

mkdir x264_/

if [ $OS_TYPE = "BSD" ]
then
	gmake -j $NUM_CPU_CORES
	gmake install
	BASH_BIN=/usr/local/bin/bash
else
	make -j $NUM_CPU_CORES
	make install
	BASH_BIN=/bin/bash
fi

cd ~

tar -xjf x264-snapshot-20191217-2245.tar.bz2
cd x264-snapshot-20191217-2245
PATH="$HOME/x264_/bin:$PATH" $BASH_BIN configure --prefix=$HOME/x264_/ --disable-opencl  --enable-pic --enable-shared
if [ $OS_TYPE = "BSD" ]
then
	PATH="$HOME/x264_/bin:$PATH" gmake -j $NUM_CPU_CORES
	gmake install
else
	PATH="$HOME/x264_/bin:$PATH" make -j $NUM_CPU_CORES
	make install
fi
echo $? > ~/install-exit-status
cd ~
rm -rf x264-snapshot-20191217-2245

7z x Bosphorus_1920x1080_120fps_420_8bit_YUV_Y4M.7z

echo "#!/bin/bash
set -x
for i in \$(seq 1 \$NUM_CPU_CORES); do 
	./x264_/bin/x264 -o /dev/null --slow --threads 1 Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m > /tmp/\${i}.log 2>&1 &
done

while [ \`ps ax | grep \"x264_/bin/x264\" | wc -l\` -gt 1 ]; do
	sleep 1
done

fps_sum=0
kbps_sum=0
for i in \$(seq 1 \${NUM_CPU_CORES}); do
	fps=\`tail -n1 /tmp/\${i}.log | cut -d\",\" -f2 | tr -dc '[. [:digit:]]'\`
	fps_sum=\`echo \"\${fps}+\${fps_sum}\" | bc -l\`
	kbps=\`tail -n1 /tmp/\${i}.log | cut -d\",\" -f3 | tr -dc '[. [:digit:]]'\`
	kbps_sum=\`echo \"\${kbps}+\${kbps_sum}\" | bc -l\`
done


echo \"encoded 600 frames, \${fps_sum} fps, \${kbps_sum} kb/s\" > \$LOG_FILE
echo \$? > ~/test-exit-status" > x264-ampere
chmod +x x264-ampere
