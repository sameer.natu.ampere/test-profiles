#!/bin/sh

tar -xzf redis-6.0.9.tar.gz

cd ~/redis-6.0.9/deps
make hiredis jemalloc linenoise lua

cd ~/redis-6.0.9
make MALLOC=libc -j $NUM_CPU_CORES
echo $? > ~/install-exit-status

cd ~
echo "#!/bin/bash
rm -rf /tmp/redis_*
#cd ~/redis-6.0.9
set -x
upto=$((${NUM_CPU_CORES}-1))
for i in \$(seq 0 \$upto);do
	./redis-6.0.9/src/redis-server --port  \$((6389+\${i})) &
done
#REDIS_SERVER_PID=\$!
sleep 60
upto=$((${NUM_CPU_CORES}-2))
for i in \$(seq 0 \$upto);do
	./redis-6.0.9/src/redis-benchmark -p \$((6389+\${i})) \$@ > /tmp/redis_\${i} &
done
final_number=$((${NUM_CPU_CORES}-1))
./redis-6.0.9/src/redis-benchmark -p \$((6388+\${NUM_CPU_CORES})) \$@ > /tmp/redis_\${final_number}
sleep 30

killall redis-server
sum=0
upto=$((${NUM_CPU_CORES}-1))
for i in \$(seq 0 \$upto);do
	sed \"s/\\\"/ /g\" -i /tmp/redis_\${i}
	score=\`cat /tmp/redis_\${i} | cut -d\",\" -f2 | cut -d\".\" -f1\`
	sum=\$((\${sum}+\${score}))
done
type=\`cat /tmp/redis_\${i} | cut -d\",\" -f1\`
sleep 10 
echo \" \${type} , \${sum}\" > \$LOG_FILE"> redis-ampere

#sed \"s/\\\"/ /g\" -i \$LOG_FILE" > redis
chmod +x redis-ampere
