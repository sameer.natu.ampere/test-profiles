#!/bin/sh

7z x Bosphorus_1920x1080_120fps_420_8bit_YUV_Y4M.7z
tar -xf rav1e-0.4.0.tar.gz
cd rav1e-0.4.0/
cargo build --bin rav1e --release -j $NUM_CPU_PHYSICAL_CORES
echo $? > ~/install-exit-status

cd ~
mkdir -p rav1e-0.4.0/target/release/
if [ ! -x rav1e-0.4.0/target/release/rav1e ]; then
    if [ $OS_ARCH = "aarch64" ]
    then
	tar -xf rav1e-0.4.0-aarch64-linux.tar.gz
	mv rav1e rav1e-0.4.0/target/release/rav1e
	echo 0 > ~/install-exit-status
    else
	tar -xf rav1e-0.4.0-linux.tar.gz
	mv rav1e rav1e-0.4.0/target/release/rav1e
	echo 0 > ~/install-exit-status
    fi
fi

cd ~

echo "#!/bin/bash
FT=\$NUM_CPU_CORES
TT=4
logfile=/tmp/rav1e

if [[ \"\$@\" == *\"-s 10 \"* ]]; then
	LOOP_CNT=60
	FRAMES=90
elif [[ \"\$@\" == *\"-s 6 \"* ]]; then
	LOOP_CNT=30
	FRAMES=60
elif [[ \"\$@\" == *\"-s 5 \"* ]]; then
	LOOP_CNT=30
	FRAMES=60
elif [[ \"\$@\" == *\"-s 1 \"* ]]; then
	LOOP_CNT=20
	FRAMES=20
fi

cd rav1e-0.4.0/

for i in \$(seq 1 \${LOOP_CNT}); do
#	./dav1d-0.8.2/build/tools/dav1d \$@ --muxer null --framethreads \${FT} --tilethreads \${TT} --filmgrain 0 > \${logfile}\${i} 2>&1 &
	./target/release/rav1e ../Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m --threads \$NUM_CPU_CORES --tiles 4 --output /dev/null \$@ > \${logfile}\${i} 2>&1 &
done

while [ \`ps ax | grep \"target/release/rav1e\" | wc -l\` -gt 1 ]; do
	        sleep 1
done
#./target/release/rav1e ../Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m --threads \$NUM_CPU_CORES --tiles 4 --output /dev/null \$@ > log.out 2>&1
echo \$? > ~/test-exit-status

fps_sum=0
kbps_sum=0
for i in \$(seq 1 \${LOOP_CNT}); do
	cp \${logfile}\${i} /tmp/tmpfile
	tr -s '\r' '\n' < /tmp/tmpfile > \${logfile}\${i}
	fps=\`cat \${logfile}\${i} | grep \">  encoded \${FRAMES}/\${FRAMES} frames\" | cut -d, -f2 | sed 's/[^0-9.]*//g'\`
	fps_sum=\`echo \"\${fps}+\${fps_sum}\" | bc -l\`
	kbps=\`cat \${logfile}\${i} | grep \">  encoded \${FRAMES}/\${FRAMES} frames\" | cut -d, -f3 | sed 's/[^0-9.]*//g'\`
	kbps_sum=\`echo \"\${kbps}+\${kbps_sum}\" | bc -l\`
done

echo \"encoded \${FRAMES}/\${FRAMES} frames, \${fps_sum} fps, \${kbps} Kb/s, est. size: 0.82 MB, est. time: 0s\" > \$LOG_FILE" > rav1e-ampere
chmod +x rav1e-ampere
