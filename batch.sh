#!/bin/bash

TIMESTAMP=`date +"%Y-%m-%d-%H-%M-%S"`
export FORCE_TIMES_TO_RUN=1

echo "starting aom-av1" > status.log
TEST_RESULTS_NAME="aom-av1-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="aom-av1-ampere" TEST_RESULTS_DESCRIPTION="aom-av1-ampere" PRESET_OPTIONS="aom-av1-ampere.enc-mode=0;aom-av1-ampere.input=1" phoronix-test-suite debug-benchmark aom-av1-ampere

echo "starting apache-ampere" > status.log
TEST_RESULTS_NAME="apache-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="apache-ampere" TEST_RESULTS_DESCRIPTION="apache-ampere" PRESET_OPTIONS="" phoronix-test-suite debug-benchmark apache-ampere

echo "starting compress-gzip" >> status.log
TEST_RESULTS_NAME="compress-gzip-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="compress-gzip-ampere" TEST_RESULTS_DESCRIPTION="compress-gzip-ampere" PRESET_OPTIONS="" phoronix-test-suite debug-benchmark compress-gzip-ampere

echo "starting dav1d" >> status.log
TEST_RESULTS_NAME="dav1d-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="dav1d-ampere-summernature1080p" TEST_RESULTS_DESCRIPTION="dav1d-ampere-summernature1080p" PRESET_OPTIONS="dav1d-ampere.video=Summer Nature 1080p" phoronix-test-suite debug-benchmark dav1d-ampere

echo "starting ffmpeg" >> status.log
TEST_RESULTS_NAME="ffmpeg-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="ffmpeg-ampere" TEST_RESULTS_DESCRIPTION="ffmpeg-ampere" PRESET_OPTIONS="" phoronix-test-suite debug-benchmark ffmpeg-ampere

echo "starting keydb" >> status.log
TEST_RESULTS_NAME="keydb-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="keydb-ampere" TEST_RESULTS_DESCRIPTION="keydb-ampere" PRESET_OPTIONS="" phoronix-test-suite debug-benchmark keydb-ampere

echo "starting mcperf" >> status.log
TEST_RESULTS_NAME="mcperf-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="mcperf-ampere-get-0" TEST_RESULTS_DESCRIPTION="mcperf-ampere-get-0" PRESET_OPTIONS="mcperf-ampere.method=get;mcperf-ampere.connections=0" phoronix-test-suite debug-benchmark mcperf-ampere

echo "starting mysqlslap" >> status.log
TEST_RESULTS_NAME="mysqlslap-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="mysqlslap-ampere-1024" TEST_RESULTS_DESCRIPTION="mysqlslap-ampere-1024" PRESET_OPTIONS="mysqlslap-ampere.clients=1024" phoronix-test-suite debug-benchmark mysqlslap-ampere

echo "starting nginx" >> status.log
TEST_RESULTS_NAME="nginx-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="nginx-ampere" TEST_RESULTS_DESCRIPTION="nginx-ampere" PRESET_OPTIONS="" phoronix-test-suite debug-benchmark nginx-ampere

echo "starting pgbench" >> status.log
TEST_RESULTS_NAME="pgbench-ampere2-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="pgbench-ampere-RO" TEST_RESULTS_DESCRIPTION="pgbench-ampere-RO" PRESET_OPTIONS="pgbench-ampere.scaling-factor=100;pgbench-ampere.clients=4000;pgbench-ampere.run-mode=Read Only" phoronix-test-suite debug-benchmark pgbench-ampere

echo "starting rav1e-1" >> status.log
TEST_RESULTS_NAME="rav1e-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="rav1e-ampere-1" TEST_RESULTS_DESCRIPTION="rav1e-ampere-1" PRESET_OPTIONS="rav1e-ampere.s=1" phoronix-test-suite debug-benchmark rav1e-ampere

echo "starting redis-get" >> status.log
TEST_RESULTS_NAME="redis-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="redis-ampere-get" TEST_RESULTS_DESCRIPTION="redis-ampere-get" PRESET_OPTIONS="redis-ampere.test=get" phoronix-test-suite debug-benchmark redis-ampere

echo "starting x264" >> status.log
TEST_RESULTS_NAME="x264-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="x264-ampere" TEST_RESULTS_DESCRIPTION="x264-ampere" PRESET_OPTIONS="" phoronix-test-suite debug-benchmark x264-ampere

echo "starting x265-1K" >> status.log
TEST_RESULTS_NAME="x265-ampere-${TIMESTAMP}" TEST_RESULTS_IDENTIFIER="x265-ampere-bosphorus1080p" TEST_RESULTS_DESCRIPTION="x265-ampere-bosphorus1080p" PRESET_OPTIONS="x265-ampere.video=Bosphorus 1080p" phoronix-test-suite debug-benchmark x265-ampere


