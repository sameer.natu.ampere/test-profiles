Setup Phoronix test suite as mentioned on the official page : git clone https://github.com/phoronix-test-suite/phoronix-test-suite.git 

- Run a base test; e.g. phoronix-test-suite benchmark x264
- This should create a .phoronix-test-suite folder in the /home/`whoami`/
- Git clone this repository inside the test-profiles/local/
git clone git@gitlab.com:sameer.natu.ampere/test-profiles.git /home/`whoami`/.phoronix_test_suite/test-profiles/local

- Now, the setup is now ready.
Please try : phoronix-test-suite benchmark x264-ampere and check htop to find if all the cores go to 100%.

Copy user-config.xml to /home/`whoami`/.phoronix_test_suite/

phoronix-test-suite install aom-av1-ampere apache-ampere compress-gzip-ampere dav1d-ampere ffmpeg-ampere keydb-ampere mcperf-ampere mysqlslap-ampere nginx-ampere pgbench-ampere rav1e-ampere redis-ampere x264-ampere x265-ampere
