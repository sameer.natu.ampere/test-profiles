#!/bin/sh

tar -xzf KeyDB-6.0.16.tar.gz

cd ~/KeyDB-6.0.16/deps
make hiredis jemalloc linenoise lua

cd ~/KeyDB-6.0.16
make MALLOC=libc -j $NUM_CPU_CORES
echo $? > ~/install-exit-status

cd ~
tar -xf memtier_benchmark-1.3.0.tar.gz
cd memtier_benchmark-1.3.0/
autoreconf -ivf
./configure
make -j $NUM_CPU_CORES
echo $? > ~/install-exit-status

cd ~
echo "#!/bin/bash
cd ~/KeyDB-6.0.16/
./src/keydb-server --server-threads 16 &
KEYDB_SERVER_PID_1=\$!
./src/keydb-server --server-threads 16 --port 1111 &
KEYDB_SERVER_PID=\$!
./src/keydb-server --server-threads 16 --port 2222 &
KEYDB_SERVER_PID_2=\$!
./src/keydb-server --server-threads 16 --port 3333 &
KEYDB_SERVER_PID_3=\$!
./src/keydb-server --server-threads 16 --port 4444 &
KEYDB_SERVER_PID_4=\$!
sleep 8
cd ~/memtier_benchmark-1.3.0/
./memtier_benchmark --hide-histogram -t \$(($NUM_CPU_CORES/6)) \$@ --port 1111 > /tmp/keydb1.log &
./memtier_benchmark --hide-histogram -t \$(($NUM_CPU_CORES/6)) \$@ --port 2222 > /tmp/keydb2.log &
./memtier_benchmark --hide-histogram -t \$(($NUM_CPU_CORES/6)) \$@ --port 3333 > /tmp/keydb3.log &
./memtier_benchmark --hide-histogram -t \$(($NUM_CPU_CORES/6)) \$@ --port 4444 > /tmp/keydb4.log &
./memtier_benchmark --hide-histogram -t \$(($NUM_CPU_CORES/6)) \$@ > /tmp/keydb5.log

kill \$KEYDB_SERVER_PID
kill \$KEYDB_SERVER_PID_1
kill \$KEYDB_SERVER_PID_2
kill \$KEYDB_SERVER_PID_3
kill \$KEYDB_SERVER_PID_4
sleep 2

sum=0
for i in {1..5}; do
	score=\`cat /tmp/keydb\${i}.log | grep Totals | sed -e \"s/ \+/ /g\" | cut -d\" \" -f2 | cut -d. -f1\`
	sum=\`echo \"\$sum+\$score\" | bc -l\`
done

echo \"Totals     \${sum}     13833.49     36766.29     63.52700      6330.95 \" > \$LOG_FILE
sleep 2" > keydb-ampere
chmod +x keydb-ampere
