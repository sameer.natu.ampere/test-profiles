#!/bin/sh

tar -xzvf memcached-1.6.9.tar.gz
cd memcached-1.6.9
# Allow running as root for benchmark
patch -p0 <<'EOF'
--- memcached.c.orig	2018-10-03 19:10:08.431811423 -0400
+++ memcached.c	2018-10-03 19:10:22.899799908 -0400
@@ -7589,8 +7589,8 @@
     /* lose root privileges if we have them */
     if (getuid() == 0 || geteuid() == 0) {
         if (username == 0 || *username == '\0') {
-            fprintf(stderr, "can't run as root without the -u switch\n");
-            exit(EX_USAGE);
+        //    fprintf(stderr, "can't run as root without the -u switch\n");
+        //    exit(EX_USAGE);
         }
         if ((pw = getpwnam(username)) == 0) {
             fprintf(stderr, "can't find the user %s to switch to\n", username);

EOF
./configure
make
echo $? > ~/install-exit-status

cd ~
tar -xzvf mcperf-0.1.1.tar.gz
cd mcperf-0.1.1

./configure
make

cd ~

echo "#!/bin/sh
set -x
SERVER_NUM_THREADS=\$((\$NUM_CPU_CORES/2))
CLIENT_NUM_THREADS=\$((\$NUM_CPU_CORES/2))
cd ~/memcached-1.6.9
./memcached -d -t \$SERVER_NUM_THREADS
sleep 2

cd ~/mcperf-0.1.1
for i in \$( seq 1 \$CLIENT_NUM_THREADS); do
	./src/mcperf \$@ > /tmp/mcperf_\$i.log 2>&1 &
done

cnt_mcperf=\`ps ax | grep "/src/mcperf" | wc -l\`
while [ \${cnt_mcperf} -gt 1 ]; do
	sleep 2
	cnt_mcperf=\`ps ax | grep "/src/mcperf"| wc -l\`
done

sudo killall memcached

kill -9 \$MEMCACHED_PID
SUM=0
for i in \$( seq 1 \$CLIENT_NUM_THREADS); do
	VAL=\`cat /tmp/mcperf_\$i.log | sed  -n -e 's/^.*rate //p' | cut -d"." -f1 \`
	SUM=\$((\$VAL+\$SUM))
done

echo \"Request rate: \$SUM req/s (0.0 ms/req)\" > \$LOG_FILE
sleep 3" > mcperf-ampere

chmod +x mcperf-ampere
