#!/bin/sh

# FFmpeg install to demux AV1 WebM to IVF that can then be consumed by dav1d...
tar -xjf ffmpeg-4.2.1.tar.bz2
mkdir ffmpeg_/

cd ffmpeg-4.2.1/
./configure --disable-zlib --disable-doc --prefix=$HOME/ffmpeg_/
make -j $NUM_CPU_CORES
echo $? > ~/install-exit-status
make install
cd ~/

./ffmpeg_/bin/ffmpeg -i Stream2_AV1_HD_6.8mbps.webm -vcodec copy -an -f ivf summer_nature_1080p.ivf
./ffmpeg_/bin/ffmpeg -i Stream2_AV1_4K_22.7mbps.webm -vcodec copy -an -f ivf summer_nature_4k.ivf
./ffmpeg_/bin/ffmpeg -i Chimera-AV1-8bit-1920x1080-6736kbps.mp4 -vcodec copy -an -f ivf chimera_8b_1080p.ivf
./ffmpeg_/bin/ffmpeg -i Chimera-AV1-10bit-1920x1080-6191kbps.mp4 -vcodec copy -an -f ivf chimera_10b_1080p.ivf

rm -rf ffmpeg-4.2.1
rm -rf ffmpeg_

# Build Dav1d
tar -xf dav1d-0.8.2.tar.xz
cd dav1d-0.8.2
meson build --buildtype release
ninja -C build
echo $? > ~/install-exit-status

cd ~

echo "#!/bin/bash
FT=\$NUM_CPU_CORES
TT=4
logfile=/tmp/dav1d

if [[ \"\$@\" == \"-isummer_nature_1080p.ivf\" ]]; then
	LOOP_CNT=\"16\"
	DIVISOR=\"25.00\"
	TOT_FRAMES=\"3604\"
elif [[ \"\$@\" == \"-isummer_nature_4k.ivf\" ]]; then
	LOOP_CNT=\"10\"
	DIVISOR=\"25.00\"
	TOT_FRAMES=\"3604\"
elif [[ \"\$@\" == \"-ichimera_8b_1080p.ivf\" ]]; then
	LOOP_CNT=\"28\"
	DIVISOR=\"23.98\"
	TOT_FRAMES=\"8929\"
elif [[ \"\$@\" == \"-ichimera_10b_1080p.ivf\" ]]; then
	LOOP_CNT=\"20\"
	DIVISOR=\"23.98\"
	TOT_FRAMES=\"8929\"
fi

for i in \$(seq 1 \${LOOP_CNT}); do
	./dav1d-0.8.2/build/tools/dav1d \$@ --muxer null --framethreads \${FT} --tilethreads \${TT} --filmgrain 0 > \${logfile}\${i} 2>&1 &
done

while [ \`ps ax | grep \"build/tools/dav1d\" | wc -l\` -gt 1 ]; do
	sleep 1
done

fps_sum=0
for i in \$(seq 1 \${LOOP_CNT}); do
	fps=\`tail -n1 /tmp/dav1d\${i} | cut -d\"-\" -f2 | cut -d\"/\" -f1 | cut -d\" \" -f2\`
	fps_sum=\`echo \"\${fps}+\${fps_sum}\" | bc -l\`
done

fps_times=\`echo \"\${fps_sum}/\${DIVISOR}\" | bc -l\`
echo \"dav1d 0.8.2 - by VideoLAN\" > \${LOG_FILE}
echo \"Decoded \${TOT_FRAMES}/\${TOT_FRAMES} frames (100.0%) - \${fps_sum}/\${DIVISOR} fps (\${fps_times}x)\" >> \${LOG_FILE}

echo \$? > ~/test-exit-status" > dav1d-ampere
chmod +x dav1d-ampere
