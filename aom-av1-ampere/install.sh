#!/bin/sh

tar -xf aom-300.tar.xz
cd aom/build
cmake -DENABLE_DOCS=0 -DENABLE_TESTS=0 -DCONFIG_AV1_DECODER=0 -DCMAKE_BUILD_TYPE=Release ..
make -j $NUM_CPU_CORES
echo $? > ~/install-exit-status
cd ~

7z x Bosphorus_1920x1080_120fps_420_8bit_YUV_Y4M.7z
rm -f Bosphorus_Copyright.txt
7z x Bosphorus_3840x2160_120fps_420_8bit_YUV_Y4M.7z

# Current AOMedia Git has MAX_NUM_THREADS value of 64, don't go over 64 threads or error

echo "#!/bin/bash
set -x
if [ \"\$NUM_CPU_CORES\" -gt 64 ]; then
	NUM_CPU_CORES=64
fi

if [[ \"\$@\" == *\"--cpu-used=9 --rt\"* ]]; then
	if [[ \"\$@\" == *\"Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m\"* ]]; then
		LOOP_CNT=50
	elif [[ \"\$@\" == *\"Bosphorus_3840x2160.y4m\"* ]]; then
		LOOP_CNT=50
	fi
elif [[ \"\$@\" == *\"--cpu-used=8 --rt\"* ]]; then
	if [[ \"\$@\" == *\"Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m\"* ]]; then
		LOOP_CNT=50
	elif [[ \"\$@\" == *\"Bosphorus_3840x2160.y4m\"* ]]; then
		LOOP_CNT=50
	fi
elif [[ \"\$@\" == *\"--cpu-used=6 --rt\"* ]]; then
	if [[ \"\$@\" == *\"Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m\"* ]]; then
		LOOP_CNT=20
	elif [[ \"\$@\" == *\"Bosphorus_3840x2160.y4m\"* ]]; then
		LOOP_CNT=20
	fi
elif [[ \"\$@\" == *\"--cpu-used=6\"* ]]; then
	if [[ \"\$@\" == *\"Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m\"* ]]; then
		LOOP_CNT=20
	elif [[ \"\$@\" == *\"Bosphorus_3840x2160.y4m\"* ]]; then
		LOOP_CNT=16
	fi
elif [[ \"\$@\" == *\"--cpu-used=4\"* ]]; then
	if [[ \"\$@\" == *\"Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m\"* ]]; then
		LOOP_CNT=16
	elif [[ \"\$@\" == *\"Bosphorus_3840x2160.y4m\"* ]]; then
		LOOP_CNT=12
	fi
elif [[ \"\$@\" == *\"--cpu-used=0 --limit=20\"* ]]; then
	if [[ \"\$@\" == *\"Bosphorus_1920x1080_120fps_420_8bit_YUV.y4m\"* ]]; then
		LOOP_CNT=12
	elif [[ \"\$@\" == *\"Bosphorus_3840x2160.y4m\"* ]]; then
		LOOP_CNT=10
	fi
fi

for i in \$(seq 1 \${LOOP_CNT}); do
	./aom/build/aomenc --threads=\$NUM_CPU_CORES -o test.av1 \$@ > \${i}.log 2>&1 &
done

while [ \`pgrep aomenc | wc -l\` -gt 1 ]; do
	sleep 1
done
echo \$? > ~/test-exit-status

sleep 2
fps_sum=0
for i in \$(seq 1 \${LOOP_CNT}); do
	cp \${i}.log /tmp/tmpfile
	tr -s '\r' '\n' < /tmp/tmpfile > \${i}.log
	fps=\`tail -n1 1.log | cut -d\"(\" -f2 | sed 's/[^0-9.]*//g'\`
	fps_sum=\`echo \"\${fps}+\${fps_sum}\" | bc -l\`
done

echo \"Pass 1/1 frame  600/600   626210B    8349b/f  250470b/s   15935 ms (\${fps_sum} fps)\" > \$LOG_FILE

rm -f test.av1" > aom-av1-ampere
chmod +x aom-av1-ampere
